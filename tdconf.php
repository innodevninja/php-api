<?php

class AppConf
{
    // token related varaiable
    const TOKEN_SECRET_KEY = "api_sec_001";
    const TOKEN_ISSUER_CLAIM = "The_Difference";
    const TOKEN_AUDIENCE_CLAIM = "The_Difference";
    const TOKEN_NOTBEFORE_CLAIM = 0;
    const TOKEN_EXPIRE_CLAIM = 300;

    // database connection info
    const DB_HOST = "localhost";
    const DB_NAME = "jwtapi";
    const DB_USER = "root";
    const DB_PASSWORD = "";

}