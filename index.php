<?php
/***
 * Entry point of this PHP web app
 * 
 * Initialize router and handle API request comes from specified URL
 * Contain all imports
 * 
 */
include_once 'tdrequest.php';
include_once 'tdrouter.php';
include_once 'tdauth.php';
include_once 'tdrest.php';

// create a router class
$router = new Router(new Request);

/*
 * login request
 * url: http://server_ip/login
 * method: post
 * parameters: userEmail, userPW, uniqueNum, userAct
 * ruturn: 
 * - success 
 * http status 200, token value
 * - failure
 * http status 401
 */
$router->post('/login', function($request) {
  // create AuthService and call login function to handle request
  $auth = new AuthService();
  return $auth->login($request->getBody());
});

/*
 * auth request
 * url: http://server_ip/auth
 * method: post
 * parameters: no
 * header: authorization (Bearer + token value)
 * ruturn: 
 * - success 
 * http status 200
 * - failure
 * http status 401
 */
$router->post('/auth', function($request) {
  $headers = apache_request_headers();
  // get header authorization value and check it is set or not
  if(isset($headers['Authorization'])){
    $authHeader = $headers['Authorization'];
    // create RestService and call checkToken function to validate token value
    $rest = new RestService();
    return $rest->checkToken($authHeader);
  } else {
    http_response_code(401);
    return json_encode(array(
        "message" => "Access denied.",
        "error" => "No authorization value"
    ));
  }
});


// list more APIs with specified URL here as same as above .... 


?>