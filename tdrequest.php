<?php
/***
 * Request class for initializing HTTP request object
 * implements IRequest interface class
 */
include_once 'tdinterface.php';

class Request implements IRequest
{
  function __construct()
  {
    $this->bootstrapSelf();
  }

  // set all keys in the global $_SERVER array as properties of this class and assigns values
  // for $_SERVER array, refer this link https://www.php.net/manual/en/reserved.variables.server.php
  private function bootstrapSelf()
  {
    foreach($_SERVER as $key => $value)
    {
      $this->{$this->toCamelCase($key)} = $value;
    }
  }

  // convert a string from snake case to camel case
  private function toCamelCase($string)
  {
    $result = strtolower($string);
        
    preg_match_all('/_[a-z]/', $result, $matches);

    foreach($matches[0] as $match)
    {
        $c = str_replace('_', '', strtoupper($match));
        $result = str_replace($match, $c, $result);
    }

    return $result;
  }

  // retrieves data from the HTTP request body
  public function getBody()
  {
    if($this->requestMethod === "GET")
    {
      return;
    }


    if ($this->requestMethod == "POST")
    {

      $body = array();
      foreach($_POST as $key => $value)
      {
        $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
      }

      return $body;
    }
  }
}
?>
