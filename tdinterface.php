<?php
/***
 * Interface class for Request class
 * for more about interface, refer this https://docs.oracle.com/javase/tutorial/java/concepts/interface.html
 */
interface IRequest
{
    public function getBody();
}
?>