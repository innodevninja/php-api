<?php
/***
 * AuthService
 * 
 * handle login, register API request
 * 
 */
header("Access-Control-Allow-Origin: * ");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once 'tddb.php';
include_once 'tdconf.php';
require "vendor/autoload.php";
use \Firebase\JWT\JWT;

Class AuthService {

    private $table_name = 'dataClient';

    public function register($parameters) {

        $databaseService = new DatabaseService();
        $conn = $databaseService->getConnection();

        $query = "INSERT INTO " . $this->table_name . "
                        SET nameFirst = :nameFirst,
                            nameLast = :nameLast,
                            userEmail = :userEmail,
                            userPW = :userPW";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(':nameFirst', $parameters["nameFirst"]);
        $stmt->bindParam(':nameLast', $parameters["nameLast"]);
        $stmt->bindParam(':userEmail', $parameters["userEmail"]);
        $userPW = hash('sha256', $parameters["userPW"]);
        $stmt->bindParam(':userPW', $userPW);

        if($stmt->execute()){
            http_response_code(200);
            echo json_encode(array("message" => "User was successfully registered."));
        }
        else{
            http_response_code(400);
            echo json_encode(array("message" => "Unable to register the user."));
        }
    }


    public function login($parameters) {

        $databaseService = new DatabaseService();
        $conn = $databaseService->getConnection();

        $query = "SELECT * FROM " . $this->table_name . " WHERE userEmail = ? AND userPW = ? LIMIT 0,1";

        $stmt = $conn->prepare($query);
        $stmt->bindParam(1, $parameters["userEmail"]);
        $pwd = hash('sha256', $parameters["userPW"]);
        $stmt->bindParam(2, $pwd);
        $stmt->execute();
        $num = $stmt->rowCount();

        if($num > 0){
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $token = $this->createJWTtoken($row);
            $jwt = JWT::encode($token, AppConf::TOKEN_SECRET_KEY);

            http_response_code(200);
                       
            echo json_encode(
                array(
                    "num" => $parameters["uniqueNum"],
                    "message" => "Successful login.",
                    "token" => $jwt,
                    "success" => 1,
                    "expireAt" => $token["exp"]
                )
            );
            
        } else {
            http_response_code(401);
            echo json_encode(array("message" => "Login failed.", "password" => $parameters["userPW"]));
        }
        
        // run procClientAuthorize procedure
        /*
        $sql = 'CALL procClientAuthorize(?, ?)';
        $stmt = $conn->prepare($sql);

        $stmt->bindParam(1, $parameters["userEmail"]);
        $stmt->bindParam(2, $parameters["userPW"]);

        $stmt->execute();

        do {
           $rows = $stmt->fetchAll(PDO::FETCH_NUM);
           if ($rows) {
               // print_r($rows);
                http_response_code(401);
               echo json_encode(
                array(
                    "message" => "Successful login."
                )
               );
           }
        } while ($stmt->nextRowset());
        */
    }

    public function createJWTtoken($row){
        $issuedat_claim = time(); // issued at
        $notbefore_claim = $issuedat_claim + AppConf::TOKEN_NOTBEFORE_CLAIM; //not before in seconds
        $expire_claim = $issuedat_claim + AppConf::TOKEN_EXPIRE_CLAIM; // expire time in seconds
        $token = array(
            "iss" => AppConf::TOKEN_ISSUER_CLAIM,
            "aud" => AppConf::TOKEN_AUDIENCE_CLAIM,
            "iat" => $issuedat_claim,
            "nbf" => $notbefore_claim,
            "exp" => $expire_claim,
            "data" => array(
                "id" => $row['recID'],
                "firstname" => $row['nameFirst'],
                "lastname" => $row['nameLast'],
                "email" => $row["userEmail"]
        ));
        return $token;
    }
}
?>