<?php
/***
 * A class to handle database connection
 */
include_once 'tdconf.php';

class DatabaseService {

    private $db_host = AppConf::DB_HOST;
    private $db_name = AppConf::DB_NAME;
    private $db_user = AppConf::DB_USER;
    private $db_password = AppConf::DB_PASSWORD;
    private $connection;

    public function getConnection(){

        $this->connection = null;

        try{
            $this->connection = new PDO("mysql:host=" . $this->db_host . ";dbname=" . $this->db_name, $this->db_user, $this->db_password);
        }catch(PDOException $exception){
            echo "Connection failed: " . $exception->getMessage();
        }

        return $this->connection;
    }
}
?>